; Drupal.org release file.
core = 7.12
api = 2

projects[advanced_help] = 1.0
projects[backup_migrate] = 2.2
projects[ctools] = 1.0-rc1
projects[entity] = 1.0-rc1
projects[globalredirect] = 1.4
projects[link] = 1.0
projects[media] = 1.0-rc3
projects[menu_block] = 2.3
projects[module_filter] = 1.6
projects[mollom] = 1.1
projects[navigation404] = 1.0
projects[pathauto] = 1.0
projects[profile2] = 1.2
projects[token] = 1.0-rc1
projects[views] = 3.1
projects[webform] = 3.15
